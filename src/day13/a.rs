pub fn run() {
    let mut lines = super::INPUT.lines();
    let mut idx = 0;
    let mut result = 0;
    while let Some(line) = lines.next() {
        idx += 1;

        let line_a = line;
        let line_b = lines.next().unwrap();
        let a = Packet::parse(line_a);
        let b = Packet::parse(line_b);
        lines.next();

        if a.root.compare(&b.root).unwrap() {
            result += idx;
        }
    }
    println!("{}", result);
}

struct Packet {
    root: Value,
}

impl Packet {
    pub fn parse(input: &str) -> Self {
        let mut stack = Vec::new();
        let mut cp = 0;
        for (idx, chr) in input.char_indices() {
            match chr {
                '[' => {
                    stack.push(Vec::new());
                    cp = idx + 1;
                }
                ',' | ']' => {
                    let literal = &input[cp..idx];
                    if !literal.is_empty() {
                        let value = Value::Int(literal.parse().unwrap());
                        stack.last_mut().unwrap().push(value);
                    }

                    if chr == ']' {
                        if stack.len() > 1 {
                            let list = stack.pop().unwrap();
                            let value = Value::List(list);
                            stack.last_mut().unwrap().push(value);
                        } else if stack.is_empty() {
                            panic!("Unmatched closing brace!");
                        }
                    }

                    cp = idx + 1;
                }
                chr if !chr.is_numeric() => panic!("Invalid char: '{}'", chr),
                _ => (),
            }
        }

        if stack.len() > 1 {
            panic!("Unmatched opening brace!");
        } else if stack.is_empty() {
            panic!("Unmatched closing brace!");
        }

        let root = Value::List(stack.pop().unwrap());
        Self { root }
    }
}

enum Value {
    Int(i32),
    List(Vec<Value>),
}

impl Value {
    pub fn compare(&self, other: &Self) -> Option<bool> {
        match (self, other) {
            (Self::Int(a), Self::Int(b)) => {
                if a < b {
                    return Some(true);
                } else if a > b {
                    return Some(false);
                }
            }
            (Self::List(a), Self::List(b)) => {
                for (a, b) in a.iter().zip(b.iter()) {
                    if let Some(result) = a.compare(b) {
                        return Some(result);
                    }
                }
                if a.len() < b.len() {
                    return Some(true);
                } else if a.len() > b.len() {
                    return Some(false);
                }
            }
            (a @ Self::Int(_), Self::List(b)) => {
                if b.is_empty() {
                    return Some(false);
                } else if let Some(result) = a.compare(&b[0]) {
                    return Some(result);
                } else if b.len() > 1 {
                    return Some(true);
                }
            }
            (Self::List(a), b @ Self::Int(_)) => {
                if a.is_empty() {
                    return Some(true);
                } else if let Some(result) = a[0].compare(b) {
                    return Some(result);
                } else if a.len() > 1 {
                    return Some(false);
                }
            }
        }

        return None;
    }
}
