pub fn run() {
    let mut packets = super::INPUT
        .lines()
        .filter(|line| !line.is_empty())
        .map(|line| Packet::parse(line, false))
        .chain([Packet::parse("[[2]]", true), Packet::parse("[[6]]", true)])
        .collect::<Vec<_>>();

    let mut swap = true;
    while swap {
        swap = false;
        for i in 0..packets.len() - 1 {
            if !packets[i].root.compare(&packets[i + 1].root).unwrap() {
                packets.swap(i, i + 1);
                swap = true;
            }
        }
    }

    let result = packets
        .iter()
        .enumerate()
        .filter(|(_, packet)| packet.divider)
        .map(|(idx, _)| idx + 1)
        .fold(1, |res, idx| res * idx);
    println!("{}", result);
}

struct Packet {
    root: Value,
    divider: bool,
}

impl Packet {
    pub fn parse(input: &str, divider: bool) -> Self {
        let mut stack = Vec::new();
        let mut cp = 0;
        for (idx, chr) in input.char_indices() {
            match chr {
                '[' => {
                    stack.push(Vec::new());
                    cp = idx + 1;
                }
                ',' | ']' => {
                    let literal = &input[cp..idx];
                    if !literal.is_empty() {
                        let value = Value::Int(literal.parse().unwrap());
                        stack.last_mut().unwrap().push(value);
                    }

                    if chr == ']' {
                        if stack.len() > 1 {
                            let list = stack.pop().unwrap();
                            let value = Value::List(list);
                            stack.last_mut().unwrap().push(value);
                        } else if stack.is_empty() {
                            panic!("Unmatched closing brace!");
                        }
                    }

                    cp = idx + 1;
                }
                chr if !chr.is_numeric() => panic!("Invalid char: '{}'", chr),
                _ => (),
            }
        }

        if stack.len() > 1 {
            panic!("Unmatched opening brace!");
        } else if stack.is_empty() {
            panic!("Unmatched closing brace!");
        }

        let root = Value::List(stack.pop().unwrap());
        Self { root, divider }
    }
}

enum Value {
    Int(i32),
    List(Vec<Value>),
}

impl Value {
    pub fn compare(&self, other: &Self) -> Option<bool> {
        match (self, other) {
            (Self::Int(a), Self::Int(b)) => {
                if a < b {
                    return Some(true);
                } else if a > b {
                    return Some(false);
                }
            }
            (Self::List(a), Self::List(b)) => {
                for (a, b) in a.iter().zip(b.iter()) {
                    if let Some(result) = a.compare(b) {
                        return Some(result);
                    }
                }
                if a.len() < b.len() {
                    return Some(true);
                } else if a.len() > b.len() {
                    return Some(false);
                }
            }
            (a @ Self::Int(_), Self::List(b)) => {
                if b.is_empty() {
                    return Some(false);
                } else if let Some(result) = a.compare(&b[0]) {
                    return Some(result);
                } else if b.len() > 1 {
                    return Some(true);
                }
            }
            (Self::List(a), b @ Self::Int(_)) => {
                if a.is_empty() {
                    return Some(true);
                } else if let Some(result) = a[0].compare(b) {
                    return Some(result);
                } else if a.len() > 1 {
                    return Some(false);
                }
            }
        }

        return None;
    }
}
