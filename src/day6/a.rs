use std::collections::vec_deque::VecDeque;

pub fn run() {
    let mut found = VecDeque::new();
    for (idx, c) in super::INPUT.char_indices() {
        if let Some(idx) = found.iter().position(|f| *f == c) {
            for _ in 0..idx + 1 {
                found.pop_front();
            }
        }

        found.push_back(c);

        if found.len() == 4 {
            println!("{}", idx + 1);
            break;
        }
    }
}
