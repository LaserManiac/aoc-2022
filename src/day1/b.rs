pub fn run() {
    let mut values = Vec::new();
    let mut sum = 0;
    for line in super::INPUT.lines() {
        if line.is_empty() {
            values.push(sum);
            sum = 0;
        } else {
            sum += line.parse::<u32>().unwrap();
        }
    }
    values.push(sum);
    values.sort();
    if let [.., a, b, c] = values.as_slice() {
        println!("{}", *a + *b + *c);
    }
}
