pub fn run() {
    let mut max = 0;
    let mut sum = 0;
    for line in super::INPUT.lines() {
        if line.is_empty() {
            max = max.max(sum);
            sum = 0;
        } else {
            sum += line.parse::<u32>().unwrap();
        }
    }
    max = max.max(sum);
    println!("{}", max);
}
