use std::collections::HashSet;

pub fn run() {
    let mut h: (i32, i32) = (0, 0);
    let mut t = h;

    let mut visited = HashSet::new();
    visited.insert(t);

    for line in super::INPUT.lines() {
        let (mx, my) = match &line[..1] {
            "L" => (-1, 0),
            "R" => (1, 0),
            "U" => (0, -1),
            "D" => (0, 1),
            _ => unimplemented!(),
        };
        let dst = line[2..].parse::<u32>().unwrap();
        for _ in 0..dst {
            h.0 += mx;
            h.1 += my;

            let dx = h.0 - t.0;
            let dy = h.1 - t.1;
            if dx.abs() > 1 || dy.abs() > 1 {
                t.0 += dx.signum();
                t.1 += dy.signum();
                visited.insert(t);
            }
        }
    }

    println!("{}", visited.len());
}
