use std::collections::HashSet;

pub fn run() {
    let mut rope = vec![(0i32, 0i32); 10];

    let mut visited = HashSet::new();
    visited.insert(rope[0]);

    let len = rope.len();
    for line in super::INPUT.lines() {
        let (mx, my) = match &line[..1] {
            "L" => (-1, 0),
            "R" => (1, 0),
            "U" => (0, -1),
            "D" => (0, 1),
            _ => unimplemented!(),
        };
        let dst = line[2..].parse::<u32>().unwrap();
        for _ in 0..dst {
            let h = &mut rope[0];
            h.0 += mx;
            h.1 += my;

            for i in 1..len {
                let h = rope[i - 1];
                let t = &mut rope[i];

                let dx = h.0 - t.0;
                let dy = h.1 - t.1;
                if dx.abs() > 1 || dy.abs() > 1 {
                    t.0 += dx.signum();
                    t.1 += dy.signum();

                    if i == len - 1 {
                        visited.insert(*t);
                    }
                }
            }
        }
    }

    println!("{}", visited.len());
}
