pub fn run() {
    let width = super::INPUT.lines().next().unwrap().len();
    let height = super::INPUT.lines().count();
    let size = width * height;
    let idx = |x: usize, y: usize| x + y * width;

    let mut field = Vec::with_capacity(size);
    for line in super::INPUT.lines() {
        field.extend(line.chars().map(|c| (c as u32 - '0' as u32) as u8))
    }

    let mut result = 0;
    for y in 0..height {
        for x in 0..width {
            let value = field[idx(x, y)];

            let mut n = 0;
            for i in 0..y {
                n += 1;
                if field[idx(x, y - 1 - i)] >= value {
                    break;
                }
            }

            let mut s = 0;
            for i in y + 1..height {
                s += 1;
                if field[idx(x, i)] >= value {
                    break;
                }
            }

            let mut w = 0;
            for i in 0..x {
                w += 1;
                if field[idx(x - 1 - i, y)] >= value {
                    break;
                }
            }

            let mut e = 0;
            for i in x + 1..width {
                e += 1;
                if field[idx(i, y)] >= value {
                    break;
                }
            }

            result = result.max(n * s * w * e);
        }
    }

    println!("{}", result);
}
