pub fn run() {
    let width = super::INPUT.lines().next().unwrap().len();
    let height = super::INPUT.lines().count();
    let size = width * height;
    let idx = |x: usize, y: usize| x + y * width;

    let mut field = Vec::with_capacity(size);
    for line in super::INPUT.lines() {
        field.extend(line.chars().map(|c| (c as u32 - '0' as u32) as u8 + 1))
    }

    let mut field_max = vec![[0 as u8; 4]; size];

    for x in 0..width {
        let mut max_n = 0;
        let mut max_s = 0;
        for y in 0..height {
            let idx_n = idx(x, y);
            let idx_s = idx(x, height - 1 - y);
            field_max[idx_n][0] = max_n;
            field_max[idx_s][1] = max_s;
            max_n = max_n.max(field[idx_n]);
            max_s = max_s.max(field[idx_s]);
        }
    }

    for y in 0..height {
        let mut max_w = 0;
        let mut max_e = 0;
        for x in 0..width {
            let idx_w = idx(x, y);
            let idx_e = idx(width - 1 - x, y);
            field_max[idx_w][2] = max_w;
            field_max[idx_e][3] = max_e;
            max_w = max_w.max(field[idx_w]);
            max_e = max_e.max(field[idx_e]);
        }
    }

    let result = field
        .into_iter()
        .enumerate()
        .filter(|(idx, value)| {
            let [n, s, w, e] = field_max[*idx];
            return n < *value || s < *value || w < *value || e < *value;
        })
        .count();
    println!("{}", result);
}
