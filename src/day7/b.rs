use std::collections::HashMap;

pub fn run() {
    let mut directories = HashMap::new();
    let mut stack = Vec::new();
    for line in super::INPUT.lines() {
        if line.starts_with(CMD_CD) {
            let dirname = &line[CMD_CD.len()..];
            if dirname == ".." {
                stack.pop();
            } else if let Some(parent) = stack.last() {
                stack.push(format!("{}/{}", parent, dirname));
            } else {
                stack.push(String::from(dirname));
            }
        }

        if let Some(Ok(size)) = line.split(' ').next().map(str::parse::<u64>) {
            for dirname in &stack {
                if !directories.contains_key(dirname) {
                    directories.insert(String::from(dirname), 0);
                }
                *directories.get_mut(dirname).unwrap() += size;
            }
        }
    }

    let required = directories["/"] - 40_000_000;
    let result = directories
        .values()
        .filter(|size| **size >= required)
        .min()
        .unwrap();
    println!("{result}");
}

const CMD_CD: &str = "$ cd ";
