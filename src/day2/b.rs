pub fn run() {
    let mut score = 0;
    for line in super::INPUT.lines() {
        let elf = 2 - ('C' as u32 - line.chars().nth(0).unwrap() as u32);
        let result = line.chars().nth(2).unwrap();
        let me = match result {
            'X' => (elf + 2) % 3,
            'Y' => elf,
            'Z' => (elf + 1) % 3,
            _ => unimplemented!(),
        };

        score += me + 1;

        if elf == me {
            score += 3;
        } else if elf == (me + 2) % 3 {
            score += 6;
        }
    }
    println!("{score}");
}
