pub fn run() {
    let mut score = 0;
    for line in super::INPUT.lines() {
        let elf = 2 - ('C' as u32 - line.chars().nth(0).unwrap() as u32);
        let me = 2 - ('Z' as u32 - line.chars().nth(2).unwrap() as u32);
        
        score += me + 1;
        
        if elf == me {
            score += 3;
        } else if elf == (me + 2) % 3 {
            score += 6;
        }
    }
    println!("{score}");
}
