pub fn run() {
    let mut x = 1i64;
    let mut cycle = 0;
    let mut result = Vec::with_capacity(40 * 6);
    for line in super::INPUT.lines() {
        let Some(op) = Op::parse(line) else {
            panic!("Invalid op: '{}'", line);
        };

        for _ in 0..op.cycles() {
            let diff = x.abs_diff((cycle % WIDTH) as i64);
            result.push(diff <= SPRITE_SIZE);
            cycle += 1;
        }

        if let Op::Add(value) = op {
            x += value;
        }
    }

    let map_px = |pixel: &bool| pixel.then_some('#').unwrap_or(' ');
    let mut row = String::with_capacity(WIDTH as _);
    for y in 0..HEIGHT {
        let from = (y * WIDTH) as usize;
        let to = ((y + 1) * WIDTH) as usize;
        let row_data = &result[from..to];
        row.extend(row_data.iter().map(map_px));
        println!("{}", row);
        row.clear();
    }
}

enum Op {
    Add(i64),
    Nop,
}

impl Op {
    pub fn parse(input: &str) -> Option<Self> {
        let mut iter = input.split(' ');
        let op = match iter.next()? {
            "addx" => Self::Add(iter.next()?.parse().ok()?),
            "noop" => Self::Nop,
            op => panic!("Invalid op: {}", op),
        };
        return Some(op);
    }

    pub fn cycles(&self) -> u64 {
        match self {
            Self::Add(..) => 2,
            Self::Nop => 1,
        }
    }
}

const WIDTH: u64 = 40;
const HEIGHT: u64 = 6;
const SPRITE_SIZE: u64 = 1;
