pub fn run() {
    let mut x = 1i64;
    let mut cycle = 0;
    let mut result = 0;
    for line in super::INPUT.lines() {
        let Some(op) = Op::parse(line) else {
            panic!("Invalid op: '{}'", line);
        };

        let start_cycle_idx = cycle_idx(cycle);
        cycle += match op {
            Op::Add(..) => 2,
            Op::Nop => 1,
        };
        let end_cycle_idx = cycle_idx(cycle);
        if start_cycle_idx != end_cycle_idx {
            let signal_cycle = idx_cycle(end_cycle_idx);
            result += signal_cycle as i64 * x;
        }

        if let Op::Add(value) = op {
            x += value;
        }
    }
    println!("{}", result);
}

fn cycle_idx(cycle: u64) -> u64 {
    ((cycle - 20) / 40 + 1) * (cycle > 20) as u64
}

fn idx_cycle(idx: u64) -> u64 {
    (idx - 1) * 40 + 20
}

enum Op {
    Add(i64),
    Nop,
}

impl Op {
    pub fn parse(input: &str) -> Option<Self> {
        let mut iter = input.split(' ');
        let op = match iter.next()? {
            "addx" => Self::Add(iter.next()?.parse().ok()?),
            "noop" => Self::Nop,
            op => panic!("Invalid op: {}", op),
        };
        return Some(op);
    }
}
