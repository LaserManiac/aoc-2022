use std::collections::HashMap;

const FREQUENCY_MULTIPLIER: u128 = 4_000_000;
const MAX_SIZE: i64 = 4_000_000;

pub fn run() {
    let sensors = super::INPUT.lines().map(Sensor::parse).collect::<Vec<_>>();

    let mut x_coverage_cache = HashMap::new();
    for y in 0..MAX_SIZE {
        let y_coverage = get_coverage_inv(&sensors, y, Sensor::get_range_y);
        for range in y_coverage {
            for x in range.from..=range.to {
                let x_coverage = x_coverage_cache
                    .entry(x)
                    .or_insert_with(|| get_coverage_inv(&sensors, x, Sensor::get_range_x));
                if x_coverage.iter().any(|range| range.contains(y)) {
                    let frequency = x as u128 * FREQUENCY_MULTIPLIER + y as u128;
                    println!("{}", frequency);
                    return;
                }
            }
        }
    }
}

fn get_coverage_inv(
    sensors: &[Sensor],
    position: i64,
    range_fn: impl Fn(&Sensor, i64) -> Option<SensorRange>,
) -> Vec<SensorRange> {
    let mut ranges = sensors
        .iter()
        .filter_map(|sensor| range_fn(sensor, position))
        .collect::<Vec<_>>();
    ranges.sort_by_key(|range| range.from);

    let mut covered = Vec::new();
    'outer: for range in ranges {
        if covered.is_empty() {
            covered.push(range);
        } else {
            for covered in &mut covered {
                if let Some(new) = covered.merge(&range) {
                    *covered = new;
                    continue 'outer;
                }
            }
            covered.push(range);
        }
    }

    let mut free = Vec::new();
    let mut idx = 0;
    for covered in covered
        .into_iter()
        .filter(|covered| covered.to > 0 && covered.from < MAX_SIZE)
    {
        if idx < covered.from {
            free.push(SensorRange {
                from: idx,
                to: covered.from - 1,
            });
        }
        idx = covered.to + 1;
    }
    if idx <= MAX_SIZE {
        free.push(SensorRange {
            from: idx,
            to: MAX_SIZE,
        });
    }

    return free;
}

struct Sensor {
    x: i64,
    y: i64,
    dst: i64,
}

impl Sensor {
    pub fn parse(input: &str) -> Self {
        let mut nums = input.split(", ").flat_map(|line| line.split(": "));
        let [sx, sy, bx, by] = std::array::from_fn(|_| {
            let num = nums.next().unwrap();
            let idx = num.rfind('=').unwrap();
            return num[idx + 1..].parse::<i64>().unwrap();
        });
        return Self {
            x: sx,
            y: sy,
            dst: (bx - sx).abs() + (by - sy).abs(),
        };
    }

    pub fn get_range_x(&self, x: i64) -> Option<SensorRange> {
        let x_diff = (x - self.x).abs();
        let y_half_range = self.dst - x_diff;
        if y_half_range >= 0 {
            Some(SensorRange {
                from: self.y - y_half_range,
                to: self.y + y_half_range,
            })
        } else {
            None
        }
    }

    pub fn get_range_y(&self, y: i64) -> Option<SensorRange> {
        let y_diff = (y - self.y).abs();
        let x_half_range = self.dst - y_diff;
        if x_half_range >= 0 {
            Some(SensorRange {
                from: self.x - x_half_range,
                to: self.x + x_half_range,
            })
        } else {
            None
        }
    }
}

struct SensorRange {
    from: i64,
    to: i64,
}

impl SensorRange {
    pub fn merge(&self, other: &Self) -> Option<Self> {
        if self.to >= other.from && self.from <= other.to {
            Some(SensorRange {
                from: self.from.min(other.from),
                to: self.to.max(other.to),
            })
        } else {
            None
        }
    }

    pub fn contains(&self, pos: i64) -> bool {
        self.from <= pos && self.to >= pos
    }
}
