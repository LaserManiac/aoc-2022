use std::collections::HashSet;

const Y_POS: i64 = 2000000;

pub fn run() {
    let sensors = super::INPUT.lines().map(Sensor::parse).collect::<Vec<_>>();

    let beacons = sensors
        .iter()
        .map(|sensor| (sensor.beacon_x, sensor.beacon_y))
        .collect::<HashSet<_>>();

    let mut ranges = sensors
        .iter()
        .filter_map(|sensor| sensor.get_range(Y_POS))
        .collect::<Vec<_>>();
    ranges.sort_by_key(|range| range.from);

    let mut covered = Vec::new();
    'outer: for range in ranges {
        if covered.is_empty() {
            covered.push(range);
        } else {
            for covered in &mut covered {
                if let Some(new) = covered.merge(&range) {
                    *covered = new;
                    continue 'outer;
                }
            }
            covered.push(range);
        }
    }

    let covered_count: u64 = covered.iter().map(SensorRange::position_count).sum();
    let beacon_count = beacons
        .into_iter()
        .filter(|(x, y)| *y == Y_POS && covered.iter().any(|range| range.contains(*x)))
        .count();
    println!("{}", covered_count - beacon_count as u64);
}

struct Sensor {
    x: i64,
    y: i64,
    beacon_x: i64,
    beacon_y: i64,
    dst: i64,
}

impl Sensor {
    pub fn parse(input: &str) -> Self {
        let mut nums = input.split(", ").flat_map(|line| line.split(": "));
        let [sx, sy, bx, by] = std::array::from_fn(|_| {
            let num = nums.next().unwrap();
            let idx = num.rfind('=').unwrap();
            return num[idx + 1..].parse::<i64>().unwrap();
        });
        return Self {
            x: sx,
            y: sy,
            beacon_x: bx,
            beacon_y: by,
            dst: (bx - sx).abs() + (by - sy).abs(),
        };
    }

    pub fn get_range(&self, y: i64) -> Option<SensorRange> {
        let y_diff = (y - self.y).abs();
        let x_half_range = self.dst - y_diff;
        if x_half_range >= 0 {
            Some(SensorRange {
                from: self.x - x_half_range,
                to: self.x + x_half_range,
            })
        } else {
            None
        }
    }
}

struct SensorRange {
    from: i64,
    to: i64,
}

impl SensorRange {
    pub fn merge(&self, other: &Self) -> Option<Self> {
        if self.to >= other.from && self.from <= other.to {
            Some(SensorRange {
                from: self.from.min(other.from),
                to: self.to.max(other.to),
            })
        } else {
            None
        }
    }

    pub fn contains(&self, pos: i64) -> bool {
        self.from <= pos && self.to >= pos
    }

    pub fn position_count(&self) -> u64 {
        (self.to - self.from + 1) as u64
    }
}
