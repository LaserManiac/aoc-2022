use std::collections::HashMap;

pub fn run() {
    let jet_pattern = super::INPUT
        .trim()
        .chars()
        .map(Dir::parse)
        .collect::<Vec<_>>();

    let looping = |period: usize| {
        let mut idx = 0;
        move || {
            let new_idx = (idx + 1) % period;
            return std::mem::replace(&mut idx, new_idx);
        }
    };
    let mut next_jet = looping(jet_pattern.len());
    let mut next_shape = looping(SHAPES.len());

    let mut cave = Cave::new();
    let mut max = 0;
    let mut iter_cache = Vec::new();
    let mut period_cache = HashMap::new();
    let mut idx = 0;
    let result = loop {
        let shape = next_shape();
        let mut x = 2;
        let mut y = max + 3;
        let jet = loop {
            let jet = next_jet();
            let dir = jet_pattern[jet];

            let new_x = x + dir.offset();
            if cave.is_free(new_x, y, SHAPES[shape]) {
                x = new_x;
            }

            let new_y = y - 1;
            if cave.is_free(x, new_y, SHAPES[shape]) {
                y = new_y;
                continue;
            }

            let top = cave.paste(x, y, shape);
            max = max.max(top + 1);
            iter_cache.push(max);
            break jet;
        };

        if shape == 0 {
            match period_cache.get(&jet) {
                None => {
                    period_cache.insert(jet, idx);
                }
                Some(prev_idx) => {
                    let prev_max = iter_cache[*prev_idx];
                    let remaining_iterations = ITERATIONS - prev_idx;
                    let period_iters = idx - prev_idx;
                    let period_height = max - prev_max;
                    let period_count = remaining_iterations / period_iters;
                    let header = prev_max as usize - 1;
                    let body = period_height as usize * period_count;
                    let footer_iterations = remaining_iterations % period_iters;
                    let footer = (iter_cache[prev_idx + footer_iterations] - prev_max) as usize;
                    break header + body + footer;
                }
            }
        }

        idx += 1;
    };
    println!("{result}");
}

struct Cave {
    rows: Vec<[bool; WIDTH]>,
}

impl Cave {
    pub fn new() -> Self {
        Self {
            rows: Default::default(),
        }
    }

    pub fn is_free(&self, x: i64, y: i64, shape: &[Pos]) -> bool {
        for [dx, dy] in shape {
            let x = x + *dx;
            let y = y + *dy;

            if x < 0 || x >= WIDTH as i64 || y < 0 {
                return false;
            }

            if self
                .rows
                .get(y as usize)
                .map(|row| row[x as usize])
                .unwrap_or(false)
            {
                return false;
            }
        }

        return true;
    }

    pub fn paste(&mut self, x: i64, y: i64, shape: usize) -> i64 {
        let top = SHAPES[shape].iter().map(|[_, dy]| y + *dy).max().unwrap();

        let required_capacity = top as usize + 1;
        let additional_capacity = required_capacity.saturating_sub(self.rows.len());

        let iter = std::iter::repeat([false; WIDTH]).take(additional_capacity);
        self.rows.extend(iter);

        for [dx, dy] in SHAPES[shape] {
            let x = (x + *dx) as usize;
            let y = (y + *dy) as usize;
            self.rows[y][x] = true;
        }

        return top;
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum Dir {
    Left,
    Right,
}

impl Dir {
    pub fn parse(input: char) -> Self {
        match input {
            '<' => Self::Left,
            '>' => Self::Right,
            _ => unimplemented!(),
        }
    }

    pub fn offset(&self) -> i64 {
        match self {
            Self::Left => -1,
            Self::Right => 1,
        }
    }
}

type Pos = [i64; 2];

const ITERATIONS: usize = 1_000_000_000_000;

const WIDTH: usize = 7;

const SHAPES: &[&[Pos]] = &[
    &[[0, 0], [1, 0], [2, 0], [3, 0]],
    &[[1, 0], [0, 1], [1, 1], [2, 1], [1, 2]],
    &[[0, 0], [1, 0], [2, 0], [2, 1], [2, 2]],
    &[[0, 0], [0, 1], [0, 2], [0, 3]],
    &[[0, 0], [1, 0], [0, 1], [1, 1]],
];
