pub fn run() {
    let pattern = super::INPUT.trim().chars().collect::<Vec<_>>();
    let mut grid = Vec::new();
    let mut jet_idx = 0;
    let mut max = 0;
    for shape_idx in 0..2022 {
        let shape = SHAPES[shape_idx % SHAPES.len()];
        let mut x = 2;
        let mut y = max + 3;
        loop {
            let push = pattern[jet_idx % pattern.len()];
            jet_idx += 1;

            let new_x = match push {
                '>' if shape.iter().all(|[dx, _]| x + *dx < WIDTH - 1) => Some(x + 1),
                '<' if shape.iter().all(|[dx, _]| x + *dx > 0) => Some(x - 1),
                '>' | '<' => None,
                _ => unimplemented!(),
            };

            if let Some(new_x) = new_x {
                let free = !shape.iter().any(|[dx, dy]| {
                    let x = new_x + *dx;
                    let y = y + *dy;
                    let idx = (y * WIDTH + x) as usize;
                    return *grid.get(idx).unwrap_or(&false);
                });
                if free {
                    x = new_x;
                }
            }

            let stop = shape.iter().any(|[_, dy]| y + *dy == 0)
                || shape.iter().any(|[dx, dy]| {
                    let x = x + *dx;
                    let y = y + *dy - 1;
                    let idx = (y * WIDTH + x) as usize;
                    return *grid.get(idx).unwrap_or(&false);
                });

            if stop {
                let cur_max = shape.iter().map(|[_, dy]| y + *dy).max().unwrap();
                let required_capacity = (cur_max as usize + 1) * WIDTH as usize;
                let additional_capacity = required_capacity.saturating_sub(grid.len());
                grid.extend(std::iter::repeat(false).take(additional_capacity));

                for [dx, dy] in shape {
                    let x = x + *dx;
                    let y = y + *dy;
                    let idx = (y * WIDTH + x) as usize;
                    grid[idx] = true;
                }

                max = max.max(cur_max + 1);
                break;
            }

            y -= 1;
        }
    }
    println!("{}", max);
}

const WIDTH: u32 = 7;

const SHAPES: &[&[[u32; 2]]] = &[
    &[[0, 0], [1, 0], [2, 0], [3, 0]],
    &[[1, 0], [0, 1], [1, 1], [2, 1], [1, 2]],
    &[[0, 0], [1, 0], [2, 0], [2, 1], [2, 2]],
    &[[0, 0], [0, 1], [0, 2], [0, 3]],
    &[[0, 0], [1, 0], [0, 1], [1, 1]],
];
