use std::collections::{HashMap, HashSet, VecDeque};

pub fn run() {
    let valves = super::INPUT
        .lines()
        .map(Valve::parse)
        .collect::<HashMap<_, _>>();
    let potential = |valve: &str, time: u32| (MAX_TIME - time) * valves[valve].flow_rate;

    const MAX_TIME: u32 = 30;
    let mut max_pressure = 0;
    let mut to_visit = VecDeque::new();
    let mut best = HashMap::new();
    to_visit.push_back(("AA", 0, 0, false, HashSet::new()));
    to_visit.push_back(("AA", 0, 0, true, HashSet::new()));
    while let Some((valve, pressure, time, open, mut opened)) = to_visit.pop_front() {
        let should_open = open && !opened.contains(valve);
        let pressure = should_open
            .then(|| pressure + potential(valve, time + 1))
            .unwrap_or(pressure);
        let time = open.then(|| time + 1).unwrap_or(time);

        if should_open {
            opened.insert(valve);
        }

        if time < MAX_TIME {
            if best.get(valve).map(|best| *best < pressure).unwrap_or(true) {
                best.insert(valve, pressure);
                let time = time + 1;
                let neighbours = &valves[valve].neighbours;
                for neighbour in neighbours {
                    to_visit.push_back((neighbour, pressure, time, false, opened.clone()));
                    to_visit.push_back((neighbour, pressure, time, true, opened.clone()));
                }
            }
        }

        max_pressure = max_pressure.max(pressure);
    }
    println!("{}", max_pressure);
}

struct Valve<'a> {
    flow_rate: u32,
    neighbours: Vec<&'a str>,
}

impl<'a> Valve<'a> {
    pub fn parse(input: &'a str) -> (&'a str, Self) {
        let name = input.split(' ').skip(1).next().unwrap();
        let flow_rate = input
            .split(';')
            .flat_map(|input| input.split('='))
            .skip(1)
            .next()
            .unwrap()
            .parse()
            .unwrap();
        let neighbours = input
            .split("valve")
            .skip(1)
            .flat_map(|input| input[1..].trim().split(", "))
            .collect();
        let valve = Self {
            flow_rate,
            neighbours,
        };
        return (name, valve);
    }
}
