use std::collections::{HashMap, HashSet, VecDeque};

const MAX_TIME: u32 = 26;

pub fn run() {
    let valves = super::INPUT
        .lines()
        .map(Valve::parse)
        .collect::<HashMap<_, _>>();

    let mut me_max = 0;
    let mut me_visit = VecDeque::new();
    let mut me_best = HashMap::new();
    me_visit.push_back(Frame::new(&valves["AA"], false, Default::default()));
    me_visit.push_back(Frame::new(&valves["AA"], true, Default::default()));

    while let Some(mut frame) = me_visit.pop_front() {
        if frame.finished() {
            continue;
        } else {
            frame.try_open();
        }

        let el_max = {
            let mut el_max = 0;
            let mut el_visit = VecDeque::new();
            let mut el_best = HashMap::new();
            el_visit.push_back(Frame::new(&valves["AA"], false, frame.opened.clone()));
            el_visit.push_back(Frame::new(&valves["AA"], true, frame.opened.clone()));

            while let Some(mut frame) = el_visit.pop_front() {
                if frame.finished() {
                    continue;
                } else {
                    frame.try_open();
                }

                el_max = el_max.max(frame.pressure);

                if el_best
                    .get(frame.valve.name)
                    .map(|best| *best < frame.pressure)
                    .unwrap_or(true)
                {
                    el_best.insert(frame.valve.name, frame.pressure);

                    for neighbour in &frame.valve.neighbours {
                        for should_open in [false, true] {
                            el_visit.push_back(Frame {
                                valve: &valves[neighbour],
                                should_open,
                                time: frame.time + 1,
                                pressure: frame.pressure,
                                opened: frame.opened.clone(),
                            });
                        }
                    }
                }
            }

            el_max
        };

        let pressure = frame.pressure + el_max;
        me_max = me_max.max(pressure);

        if me_best
            .get(frame.valve.name)
            .map(|best| *best < pressure)
            .unwrap_or(true)
        {
            me_best.insert(frame.valve.name, pressure);

            for neighbour in &frame.valve.neighbours {
                for should_open in [false, true] {
                    me_visit.push_back(Frame {
                        valve: &valves[neighbour],
                        should_open,
                        time: frame.time + 1,
                        pressure: frame.pressure,
                        opened: frame.opened.clone(),
                    });
                }
            }
        }
    }
    println!("{}", me_max);
}

struct Frame<'a> {
    valve: &'a Valve<'a>,
    should_open: bool,
    time: u32,
    pressure: u32,
    opened: HashSet<&'a str>,
}

impl<'a> Frame<'a> {
    pub fn new(valve: &'a Valve, should_open: bool, opened: HashSet<&'a str>) -> Self {
        Self {
            valve,
            should_open,
            time: 0,
            pressure: 0,
            opened,
        }
    }

    pub fn finished(&self) -> bool {
        self.time >= MAX_TIME
    }

    pub fn try_open(&mut self) {
        if self.should_open && !self.opened.contains(self.valve.name) {
            self.time += 1;
            self.pressure += self.valve.flow_rate * MAX_TIME.saturating_sub(self.time);
            self.opened.insert(self.valve.name);
        }
    }
}

struct Valve<'a> {
    name: &'a str,
    flow_rate: u32,
    neighbours: Vec<&'a str>,
}

impl<'a> Valve<'a> {
    pub fn parse(input: &'a str) -> (&'a str, Self) {
        let name = input.split(' ').skip(1).next().unwrap();
        let flow_rate = input
            .split(';')
            .flat_map(|input| input.split('='))
            .skip(1)
            .next()
            .unwrap()
            .parse()
            .unwrap();
        let neighbours = input
            .split("valve")
            .skip(1)
            .flat_map(|input| input[1..].trim().split(", "))
            .collect();
        let valve = Self {
            name,
            flow_rate,
            neighbours,
        };
        return (name, valve);
    }
}
