pub fn run() {
    let paths = super::INPUT.lines().map(Path::parse).collect::<Vec<_>>();

    let max_y = paths
        .iter()
        .flat_map(|path| path.points.iter())
        .map(|point| point.y)
        .max()
        .unwrap();

    let min_y = 0;
    let max_y = max_y + 2;
    let min_x = 500 - max_y - 1;
    let max_x = 500 + max_y + 1;
    let width = (max_x - min_x + 1) as usize;
    let height = (max_y - min_y + 1) as usize;
    let idx = |x: i32, y: i32| y as usize * width + x as usize;

    let mut field = vec![Tile::Air; width * height];
    for x in 0..width {
        field[idx(x as i32, height as i32 - 1)] = Tile::Rock;
    }
    for path in &paths {
        for i in 1..path.points.len() {
            let a = &path.points[i - 1];
            let b = &path.points[i];
            let a_x = a.x - min_x;
            let a_y = a.y - min_y;
            let b_x = b.x - min_x;
            let b_y = b.y - min_y;

            if a_x == b_x {
                for y in a_y.min(b_y)..=a_y.max(b_y) {
                    field[idx(a_x, y)] = Tile::Rock;
                }
            } else if a_y == b_y {
                for x in a_x.min(b_x)..=a_x.max(b_x) {
                    field[idx(x, a_y)] = Tile::Rock;
                }
            }
        }
    }

    let mut count = 0;
    'outer: loop {
        let mut x = 500 - min_x;
        let mut y = 0i32;

        'sand: loop {
            let Tile::Air = field[idx(x, y)] else {
                break 'outer;
            };

            for (dx, dy) in [(0, 1), (-1, 1), (1, 1)] {
                let (nx, ny) = (x + dx, y + dy);
                if let Tile::Air = field[idx(nx, ny)] {
                    (x, y) = (nx, ny);
                    continue 'sand;
                }
            }

            field[idx(x, y)] = Tile::Sand;
            break;
        }

        count += 1;
    }

    println!("{}", count);
}

#[derive(Clone, Copy)]
enum Tile {
    Air,
    Rock,
    Sand,
}

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn parse(input: &str) -> Self {
        let comma = input.find(',').unwrap();
        let x = input[..comma].parse().unwrap();
        let y = input[comma + 1..].parse().unwrap();
        Self { x, y }
    }
}

struct Path {
    points: Vec<Point>,
}

impl Path {
    pub fn parse(input: &str) -> Self {
        let points = input.split(" -> ").map(Point::parse).collect();
        Self { points }
    }
}
