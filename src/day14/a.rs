pub fn run() {
    let paths = super::INPUT.lines().map(Path::parse).collect::<Vec<_>>();

    let (min_x, _, max_x, max_y) = paths
        .iter()
        .flat_map(|path| path.points.iter())
        .map(|point| (point.x, point.y, point.x, point.y))
        .reduce(|a, b| (a.0.min(b.0), a.1.min(b.1), a.2.max(b.2), a.3.max(b.3)))
        .unwrap();

    let (min_x, min_y) = (min_x - 1, 0);
    let width = (max_x - min_x + 2) as usize;
    let height = (max_y - min_y + 2) as usize;
    let idx = |x: i32, y: i32| y as usize * width + x as usize;

    let mut field = vec![Tile::Air; width * height];
    for path in &paths {
        for i in 1..path.points.len() {
            let a = &path.points[i - 1];
            let b = &path.points[i];
            let a_x = a.x - min_x;
            let a_y = a.y - min_y;
            let b_x = b.x - min_x;
            let b_y = b.y - min_y;

            if a_x == b_x {
                for y in a_y.min(b_y)..=a_y.max(b_y) {
                    field[idx(a_x, y)] = Tile::Rock;
                }
            } else if a_y == b_y {
                for x in a_x.min(b_x)..=a_x.max(b_x) {
                    field[idx(x, a_y)] = Tile::Rock;
                }
            }
        }
    }

    let mut count = 0;
    'outer: loop {
        let mut x = 500 - min_x;
        let mut y = 0i32;

        'sand: loop {
            if y as usize == height - 1 {
                break 'outer;
            }

            for (dx, dy) in [(0, 1), (-1, 1), (1, 1)] {
                let (nx, ny) = (x + dx, y + dy);
                if let Tile::Air = field[idx(nx, ny)] {
                    (x, y) = (nx, ny);
                    continue 'sand;
                }
            }

            field[idx(x, y)] = Tile::Sand;
            break;
        }

        count += 1;
    }

    println!("{}", count);
}

#[derive(Clone, Copy)]
enum Tile {
    Air,
    Rock,
    Sand,
}

struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn parse(input: &str) -> Self {
        let comma = input.find(',').unwrap();
        let x = input[..comma].parse().unwrap();
        let y = input[comma + 1..].parse().unwrap();
        Self { x, y }
    }
}

struct Path {
    points: Vec<Point>,
}

impl Path {
    pub fn parse(input: &str) -> Self {
        let points = input.split(" -> ").map(Point::parse).collect();
        Self { points }
    }
}
