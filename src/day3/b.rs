pub fn run() {
    let mut rucksacks = super::INPUT.lines();
    let mut result = 0;
    while let Some(a) = rucksacks.next() {
        let b = rucksacks.next().unwrap();
        let c = rucksacks.next().unwrap();
        for chr in a.chars() {
            if b.contains(chr) && c.contains(chr) {
                let chr = chr as u32;
                let value = if chr > 'Z' as u32 {
                    chr - 'a' as u32 + 1
                } else {
                    chr - 'A' as u32 + 27
                };
                result += value;
                break;
            }
        }
    }
    println!("{}", result);
}