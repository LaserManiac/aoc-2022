pub fn run() {
    let result = super::INPUT.lines()
        .map(|rucksack| {
            let half = rucksack.len() / 2;
            (&rucksack[..half], &rucksack[half..])
        })
        .map(|(comp_a, comp_b): (&str, &str)| {
            for c in comp_a.chars() {
                if comp_b.contains(c) {
                    return c;
                }
            }
            unreachable!();
        })
        .map(|c| {
            let c = c as u32;
            if c > 'Z' as u32 {
                c - 'a' as u32 + 1
            } else {
                c - 'A' as u32 + 27
            }
        })
        .sum::<u32>();
    println!("{}", result);
}