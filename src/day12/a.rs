use std::collections::HashMap;

pub fn run() {
    let width = super::INPUT.lines().next().unwrap().len();
    let height = super::INPUT.lines().count();
    let idx = |x: i32, y: i32| y as usize * width + x as usize;

    let mut map = Vec::new();
    let mut start = (0, 0);
    let mut end = (0, 0);
    for line in super::INPUT.lines() {
        map.reserve(line.len());
        for chr in line.chars() {
            let x = (map.len() % width) as i32;
            let y = (map.len() / width) as i32;
            if chr == 'S' {
                start = (x, y);
                map.push(0);
            } else if chr == 'E' {
                end = (x, y);
                map.push('z' as i32 - 'a' as i32);
            } else {
                map.push(chr as i32 - 'a' as i32);
            }
        }
    }

    let mut visited = HashMap::new();
    let mut wavefront = HashMap::new();

    visited.insert(start, 0);
    wavefront.insert(start, 0);

    let result = loop {
        let Some((pos, _)) = wavefront.iter().min_by_key(|(_, steps)| *steps) else {
            break None;
        };

        let pos = *pos;
        let steps = wavefront.remove(&pos).unwrap();
        if pos == end {
            break Some(steps);
        }

        let (x, y) = pos;
        let elevation = map[idx(x, y)];
        for (dx, dy) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            let nx = x + dx;
            let ny = y + dy;
            if nx < 0 || nx >= width as i32 || ny < 0 || ny >= height as i32 {
                continue;
            }

            let new_elevation = map[idx(nx, ny)];
            if new_elevation > elevation + 1 {
                continue;
            }

            let new_pos = (nx, ny);
            let new_steps = steps + 1;

            if visited.contains_key(&new_pos) {
                continue;
            }

            visited.insert(new_pos, new_steps);
            wavefront.insert(new_pos, new_steps);
        }
    };

    if let Some(result) = result {
        println!("{}", result);
    }
}
