pub fn run() {
    let cargo_input = super::INPUT
        .lines()
        .take_while(|line| !line.is_empty())
        .collect::<Vec<_>>()
        .join("\n");
    let command_input = super::INPUT
        .lines()
        .skip_while(|line| !line.is_empty())
        .skip(1);

    let mut cargo = Cargo::parse(&cargo_input);
    command_input
        .map(Command::parse)
        .for_each(|cmd| cargo.exec(&cmd));

    let result = cargo.get_result();
    println!("{}", result);
}

struct Cargo {
    stacks: Vec<Vec<char>>,
}

impl Cargo {
    pub fn parse(input: &str) -> Self {
        let stack_count = input
            .lines()
            .map(|line| (line.len() + 1) / 4)
            .max()
            .unwrap() as usize;
        let mut stacks = vec![Vec::new(); stack_count];

        for line in input.lines().rev() {
            let mut last_idx = 0;
            while let Some(idx) = line[last_idx..].find('[') {
                let idx = last_idx + idx;
                let stack = &mut stacks[idx / 4];
                let item = line.chars().nth(idx + 1).unwrap();
                stack.push(item);
                last_idx = idx + 1;
            }
        }

        Self { stacks }
    }

    pub fn exec(&mut self, cmd: &Command) {
        for _ in 0..cmd.items {
            let item = self.stacks[cmd.from - 1].pop().unwrap();
            self.stacks[cmd.to - 1].push(item);
        }
    }

    pub fn get_result(&self) -> String {
        self.stacks
            .iter()
            .filter_map(|stack| stack.last())
            .copied()
            .collect()
    }
}

struct Command {
    items: usize,
    from: usize,
    to: usize,
}

impl Command {
    pub fn parse(input: &str) -> Self {
        let from_idx = input.find("from ").unwrap();
        let to_idx = input.find("to ").unwrap();
        let items = input["move ".len()..from_idx - 1].parse().unwrap();
        let from = input[from_idx + "from ".len()..to_idx - 1].parse().unwrap();
        let to = input[to_idx + "to ".len()..].parse().unwrap();
        Self { items, from, to }
    }
}
