pub fn run() {
    let result = super::INPUT
        .lines()
        .map(Pair::parse)
        .filter(|pair| pair.a.contains(&pair.b) || pair.b.contains(&pair.a))
        .count();
    println!("{}", result);
}

struct Range {
    start: u32,
    end: u32,
}

impl Range {
    pub fn parse(input: &str) -> Self {
        let sep = input.find('-').unwrap();
        Self {
            start: input[..sep].parse().unwrap(),
            end: input[sep + 1..].parse().unwrap(),
        }
    }

    pub fn contains(&self, other: &Range) -> bool {
        self.start <= other.start && self.end >= other.end
    }
}

struct Pair {
    a: Range,
    b: Range,
}

impl Pair {
    pub fn parse(input: &str) -> Self {
        let sep = input.find(',').unwrap();
        Self {
            a: Range::parse(&input[..sep]),
            b: Range::parse(&input[sep + 1..]),
        }
    }
}
