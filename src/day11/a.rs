use std::collections::{HashMap, VecDeque};

pub fn run() {
    let mut monkeys = HashMap::new();
    let mut monkey_names = Vec::new();
    let mut lines = super::INPUT.lines().peekable();
    while let Some(next) = lines.peek() {
        if next.starts_with("Monkey") {
            let monkey = Monkey::parse(&mut lines).expect("Invalid monkey!");
            monkey_names.push(monkey.name);
            monkeys.insert(monkey.name, monkey);
        } else {
            let _ = lines.next();
        }
    }

    let mut throw_cache = Vec::new();
    for _ in 0..20 {
        for name in &monkey_names {
            let monkey = &mut monkeys.get_mut(name).unwrap();
            while let Some(item) = monkey.items.pop_back().clone() {
                let item = monkey.operation.resolve(item) / 3;
                let throw_to = if item % monkey.test == 0 {
                    monkey.on_true
                } else {
                    monkey.on_false
                };
                throw_cache.push((throw_to, item));
            }
            monkey.inspected += throw_cache.len() as u32;
            for (monkey, item) in throw_cache.drain(..) {
                monkeys.get_mut(&monkey).unwrap().items.push_front(item);
            }
        }
    }

    let (_, a) = monkeys
        .iter()
        .max_by_key(|(_, monkey)| monkey.inspected)
        .unwrap();
    let (_, b) = monkeys
        .iter()
        .filter(|(name, _)| **name != a.name)
        .max_by_key(|(_, monkey)| monkey.inspected)
        .unwrap();
    let result = a.inspected * b.inspected;
    println!("{}", result);
}

struct Monkey {
    name: u32,
    items: VecDeque<u32>,
    operation: Operation,
    test: u32,
    on_true: u32,
    on_false: u32,
    inspected: u32,
}

impl Monkey {
    pub fn parse<'a>(input: &mut impl Iterator<Item = &'a str>) -> Option<Self> {
        let name = input
            .next()?
            .split(':')
            .next()?
            .rsplit(' ')
            .next()?
            .parse()
            .ok()?;
        let items = input
            .next()?
            .split(':')
            .skip(1)
            .next()?
            .split(',')
            .map(str::trim)
            .map(str::parse)
            .collect::<Result<_, _>>()
            .ok()?;
        let operation = Operation::parse(input.next()?)?;
        let test = input.next()?.rsplit(' ').next()?.parse().ok()?;
        let on_true = input.next()?.rsplit(' ').next()?.parse().ok()?;
        let on_false = input.next()?.rsplit(' ').next()?.parse().ok()?;
        return Some(Self {
            name,
            items,
            operation,
            test,
            on_true,
            on_false,
            inspected: 0,
        });
    }
}

struct Operation {
    operator: Operator,
    a: Var,
    b: Var,
}

impl Operation {
    pub fn parse(input: &str) -> Option<Self> {
        let func = input.split('=').skip(1).next()?;
        let operator = Operator::parse(func)?;
        let mut vars = func.split(operator.symbol()).map(str::trim);
        let a = Var::parse(vars.next()?)?;
        let b = Var::parse(vars.next()?)?;
        return Some(Self { operator, a, b });
    }

    pub fn resolve(&self, old: u32) -> u32 {
        let a = self.a.resolve(old);
        let b = self.b.resolve(old);
        return self.operator.resolve(a, b);
    }
}

enum Operator {
    Add,
    Mul,
}

impl Operator {
    pub fn parse(input: &str) -> Option<Self> {
        if input.contains('+') {
            Some(Self::Add)
        } else if input.contains('*') {
            Some(Self::Mul)
        } else {
            None
        }
    }

    pub fn symbol(&self) -> char {
        match self {
            Self::Add => '+',
            Self::Mul => '*',
        }
    }

    pub fn resolve(&self, a: u32, b: u32) -> u32 {
        match self {
            Self::Add => a + b,
            Self::Mul => a * b,
        }
    }
}

enum Var {
    Old,
    Const(u32),
}

impl Var {
    pub fn parse(input: &str) -> Option<Self> {
        if input == "old" {
            Some(Self::Old)
        } else {
            Some(Self::Const(input.parse().ok()?))
        }
    }

    pub fn resolve(&self, old: u32) -> u32 {
        match self {
            Self::Old => old,
            Self::Const(value) => *value,
        }
    }
}
