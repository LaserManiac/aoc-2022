use std::collections::HashSet;

pub fn run() {
    let cubes = super::INPUT
        .lines()
        .map(|line| {
            let mut iter = line.split(',');
            let x = iter.next().unwrap().parse::<i32>().unwrap();
            let y = iter.next().unwrap().parse::<i32>().unwrap();
            let z = iter.next().unwrap().parse::<i32>().unwrap();
            return [x, y, z];
        })
        .collect::<HashSet<_>>();

    let [min_x, max_x, min_y, max_y, min_z, max_z] = cubes
        .iter()
        .map(|c| [c[0], c[0], c[1], c[1], c[2], c[2]])
        .reduce(|a, b| {
            [
                a[0].min(b[0]),
                a[1].max(b[1]),
                a[2].min(b[2]),
                a[3].max(b[3]),
                a[4].min(b[4]),
                a[5].max(b[5]),
            ]
        })
        .unwrap();
    let in_range = |[x, y, z]: [i32; 3]| {
        x >= min_x - 1
            && x <= max_x + 1
            && y >= min_y - 1
            && y <= max_y + 1
            && z >= min_z - 1
            && z <= max_z + 1
    };

    let mut outside = HashSet::new();
    let mut wavefront = vec![[min_x - 1, min_y - 1, min_z - 1]];
    while let Some([x, y, z]) = wavefront.pop() {
        for pos in neighbours(x, y, z) {
            if in_range(pos) && !outside.contains(&pos) && !cubes.contains(&pos) {
                outside.insert(pos);
                wavefront.push(pos);
            }
        }
    }

    let result = cubes
        .iter()
        .flat_map(|[x, y, z]| neighbours(*x, *y, *z))
        .filter(|pos| !cubes.contains(pos) && (!in_range(*pos) || outside.contains(pos)))
        .count();
    println!("{}", result);
}

fn neighbours(x: i32, y: i32, z: i32) -> [[i32; 3]; 6] {
    [
        [x + 1, y, z],
        [x - 1, y, z],
        [x, y + 1, z],
        [x, y - 1, z],
        [x, y, z + 1],
        [x, y, z - 1],
    ]
}
