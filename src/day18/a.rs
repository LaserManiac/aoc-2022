use std::collections::HashSet;

pub fn run() {
    let cubes = super::INPUT
        .lines()
        .map(|line| {
            let mut iter = line.split(',');
            let x = iter.next().unwrap().parse::<i32>().unwrap();
            let y = iter.next().unwrap().parse::<i32>().unwrap();
            let z = iter.next().unwrap().parse::<i32>().unwrap();
            return (x, y, z);
        })
        .collect::<HashSet<_>>();

    let result = cubes
        .iter()
        .flat_map(|(x, y, z)| {
            [
                (*x + 1, *y, *z),
                (*x - 1, *y, *z),
                (*x, *y + 1, *z),
                (*x, *y - 1, *z),
                (*x, *y, *z + 1),
                (*x, *y, *z - 1),
            ]
        })
        .filter(|pos| !cubes.contains(pos))
        .count();
    println!("{}", result);
}
